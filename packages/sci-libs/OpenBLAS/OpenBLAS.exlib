# Copyright 2009 Elias Pipping <pipping@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require alternatives option-renames [ renames=[ 'threads openmp' ] ]

if ever is_scm; then
    require github [ user=xianyi branch=develop ]
else
    require github [ user=xianyi tag=v${PV} ]
fi

export_exlib_phases pkg_setup src_prepare src_compile src_test src_install

SUMMARY="Optimized BLAS library based on GotoBLAS2"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    openmp [[ description = [ Enables SMP through openmp ] ]]
    cpu-agnostic [[ description = [ Work on any CPU type rather than just the current one ] ]]
    platform:
        amd64
"

DEPENDENCIES="
    build+run:
        sys-libs/libgfortran:=
        openmp? ( sys-libs/libgomp:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/shared-blas.patch
)

OpenBLAS_pkg_setup() {
    unset CFLAGS
}

OpenBLAS_src_prepare() {
    default

    # Strip everything but the version.
    edo sed -n -e '/^VERSION = /p' -i Makefile.rule

    local ARGS=(
        FC=${FORTRAN}
        NO_LAPACK=1
        NO_LAPACKE=1
        PREFIX=/usr/$(exhost --target)
        HOSTCC=$(exhost --build)-cc
    )

    if [[ $(exhost --target) == x86_64-pc-linux-* ]] ; then
        ARGS+=( BINARY=64 )
    else
        ARGS+=( BINARY=32 )
    fi

    # TODO: We should handle this better instead of ignoring the option
    # completely when cross-compiling.
    if exhost --is-native -q ; then
        ARGS+=( DYNAMIC_ARCH=$(option cpu-agnostic 1 0) )
    else
        ARGS+=( DYNAMIC_ARCH=1 )
    fi

    if [[ $(exhost --target) == armv*-unknown-linux-* ]]; then
        case "$(exhost --target)" in
            armv6*)
                ARGS+=( TARGET=ARMV6 )
                ;;

            armv7*)
                ARGS+=( TARGET=ARMV7 )
                ;;

            aarch64*)
                ARGS+=( TARGET=ARMV8 )
                ;;
        esac
    fi

    if option openmp; then
        ARGS+=( USE_THREAD=1 USE_OPENMP=1 )
    else
        ARGS+=( USE_THREAD=0 USE_OPENMP=0 )
    fi

    for ARG in "${ARGS[@]}"; do
        echo "${ARG}" 1>&2
        echo "${ARG}" >> Makefile.rule || die "Failed writing Makefile.rule"
    done
}

OpenBLAS_src_compile() {
    emake AR=${AR} libs shared
    emake -C interface libblas.so
}

OpenBLAS_src_test() {
    emake tests
}

OpenBLAS_src_install() {
    default

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin

    newlib.so interface/libblas.so libblas-openblas.so
    alternatives_for blas ${PN} 0 /usr/$(exhost --target)/lib/libblas.so libblas-openblas.so
}

