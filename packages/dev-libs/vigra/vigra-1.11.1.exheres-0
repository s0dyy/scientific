# Copyright 2008, 2010, 2013 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'vigra-1.6.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require github [ user=ukoethe tag=Version-$(ever replace_all -) ] cmake

SUMMARY="A C++ computer vision library with emphasize on customizable algorithms and data structures"
DESCRIPTION="
VIGRA stands for \"Vision with Generic Algorithms\". It's a novel computer vision library that puts
its main emphasize on customizable algorithms and data structures.
By using template techniques similar to those in the C++ Standard Template Library, you can easily
adapt any VIGRA component to the needs of your application, without thereby giving up execution speed.
"
HOMEPAGE="https://ukoethe.github.io/${PN}"

BUGS_TO="ingmar@exherbo.org"

UPSTREAM_CHANGELOG="${HOMEPAGE}/doc/vigra/CreditsChangelog.html"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    openexr [[ description = [ Adds support for reading and writing OpenEXR images ] ]]
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libpng:=
        sci-libs/fftw[>=3.3]
        sys-libs/zlib
        openexr? (
            media-libs/ilmbase
            media-libs/openexr
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-multi_convolution-Fix-for-incorrect-template-paramet.patch
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DDOCINSTALL:PATH=../share/doc/${PNVR}
    -DWITH_HDF5:BOOL=FALSE
    -DWITH_VALGRIND:BOOL=FALSE
    # Would need boost
    -DWITH_VIGRANUMPY:BOOL=FALSE
    -DWITH_BOOST_GRAPH:BOOL=FALSE
    -DWITH_BOOST_THREAD:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'doc Doxygen'
    TIFF
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=( OPENEXR )

src_prepare() {
    cmake_src_prepare

    # Disable failing tests, https://github.com/ukoethe/vigra/issues/409
    edo sed -e "/VIGRA_ADD_TEST(test_blockwiselabeling /d" \
            -e "/VIGRA_ADD_TEST(test_blockwisewatersheds /d" \
            -i test/blockwisealgorithms/CMakeLists.txt
}
