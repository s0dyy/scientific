# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require elisp-module autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.13 ] ]

SUMMARY="A Computer Algebra System"
HOMEPAGE="http://${PN}.sourceforge.net/"
DOWNLOADS="http://sourceforge.net/projects/${PN}/files/${PN^}-source/${PV}-source/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="emacs"

DEPENDENCIES="
    build+run:
        dev-lang/sbcl
    suggestion:
        emacs? (
            app-text/ghostscript [[ description = [ For output beautification ] ]]
        )
    recommendation:
        sci-apps/gnuplot
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-sbcl )

src_prepare() {
    option emacs || edo sed -e /SUBDIRS/s:emacs:: -i interfaces/Makefile.am
    autotools_src_prepare
}

src_install() {
    edo sed -e "/top_srcdir=/s|=.*\$|=/usr/share/${PN}/${PV}|" -i src/maxima
    emagicdocs
    if option emacs; then
        emake install DESTDIR="${IMAGE}" emacsdir=${ELISP_SITE_LISP}/${PN}
        elisp-install-site-file
    else
        emake install DESTDIR="${IMAGE}"
    fi
}

pkg_postinst() {
    elisp-module_pkg_postinst
}

pkg_postrm() {
    elisp-module_pkg_postrm
}

